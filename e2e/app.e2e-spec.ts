import { MosstFrontendRefactoringPage } from './app.po';

describe('mosst-frontend-refactoring App', () => {
  let page: MosstFrontendRefactoringPage;

  beforeEach(() => {
    page = new MosstFrontendRefactoringPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
