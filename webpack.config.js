const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: './server/bin/www.js',
  output: {
    path: './dist/server',
    filename: 'server.js'
  },
  target: 'node',
  node: {
    __dirname: false
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    new webpack.optimize.UglifyJsPlugin({
      // sourceMap: true,
      comments: false,
      compress: {
        warnings: false,
        drop_console: true
      },
    })]
};
