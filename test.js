const mask = '+38 (0XX) XXX XX XX';
const output = '+380XXXXXXXXX';

const allowedInputs = '[0-9]';
const allowedSymbols = new RegExp(allowedInputs);

// function getSpecialSymbols(mask) {
//   let symbols = {};
//   for (let i = 0; i < mask.length; i++) {
//     if (mask[i] !== 'X') {
//       const currentSymbol = mask[i];
//       if (!symbols[currentSymbol]) {
//         symbols[currentSymbol] = [i];
//       } else {
//         symbols[currentSymbol].push(i)
//       }
//     }
//   }
//   return symbols
// }
//
// function getSpecialSymbolsList(mask) {
//   let symbols = [];
//   for (let i = 0; i < mask.length; i++) {
//     if (mask[i] !== 'X') {
//       const currentSymbol = mask[i];
//       if (symbols.indexOf(currentSymbol) === -1 && !allowedSymbols.test(currentSymbol)) {
//         const symbolRegex = new RegExp(`\\${currentSymbol}`, 'g')
//         symbols.push(symbolRegex);
//       }
//     }
//   }
//   return symbols
// }

function removeNotAllowedSymbolsFromString(inputString = '') {
  let stringWithoutNotAllowedSymbols = '';

  for (let i = 0; i < inputString.length; i++) {
    if (allowedSymbols.test(inputString[i])) {
      stringWithoutNotAllowedSymbols += inputString[i];
    }
  }

  return stringWithoutNotAllowedSymbols;
}

function getMeaningPart(string) {
  let meaningPart = '';
  if (string.length > mask.length) {
    string = string.slice(-mask.length);
  }
  for (let i = 0; i < string.length; i++) {
    if (mask[i] == 'X') {
      meaningPart += string[i];
    }
  }
  return meaningPart;
}

function useMask(mask, string) {
  const allowedLength = mask.length - mask.replace(/X/g, '').length;
  string = removeNotAllowedSymbolsFromString(string).slice(-allowedLength);
  let curentString = mask;


  for (let i = 0; i < string.length; i++) {
    curentString = curentString.replace(/X/, string[i]);
  }

  curentString = curentString.replace(/X.*/g, '');
  return curentString;
}

function unmask(string) {
  const meaningPart = getMeaningPart(string);
  let result = output;

  for (let i = 0; i < meaningPart.length; i++) {
    result = result.replace(/X/, meaningPart[i])
  }

  result = result.replace(/X/g, '');
  return result;
}

console.log(useMask(mask, '+38 (0XX) XXX XX XX'));
console.log(useMask(mask, '+3806+6)8(5a3wr29rt85'));
console.log(useMask(mask, '066832985sdawdatey5'));
console.log(useMask(mask, '+3806'));
console.log(useMask(mask, '532985'));
console.log(useMask(mask, ''));

const masked = useMask(mask, '+380668532985');
console.log(getMeaningPart(masked));
console.log(masked)
console.log(unmask(masked));
console.log(unmask(mask));
console.log(unmask(''));
