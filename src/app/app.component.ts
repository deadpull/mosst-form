import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { IUser } from './register/interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public show: boolean;
  public type: string;

  public user: IUser = {
    profile_session_guid: '24B204ED-7882-4F68-BF79-09F0F020478A',
    profile_guid: 'AFFF09B8-C42D-4D99-8302-F03E665A1C86',
    valid_dtm: '2017-10-05T11:51:51.865Z',
    msisdn: '+380631233456',
    first_name: 'Test',
    last_name: 'Test',
    middle_name: 'Test',
    notification_type: 'ES'
  }

  locales = {
    en: {
      enter_to: 'Enter to'
    }
  };

  public config = {
    registrationUrl: '/api/user/register',
    otpCheckUrl: '/api/user/otp',
    loginUrl: '/api/user/login',
    forgotUrl: '/api/user/reset_pass',
    profileUrl: '/api/user/profile',
    avatarUrl: 'https://api-users.mosst.com/i/'
  };

  languages = ['ru', 'uk', 'en'];
  currentLang = 'ru';

  setLang(event) {
    const index = event.target.value || 0;
    this.currentLang = this.languages[index];
  }

  constructor() {
  }


  public onOverlayClick(event): void {

  }

  public onRegistrationDone(): void {
  }
}
