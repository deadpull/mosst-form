export const GMAP_ZOOM_ON_START = 11;
export const GMAP_ZOOM_ON_POINT_CLICK = 16;
// Kyiv
export const GMAP_DEFAULT_COORDINATES = {
  latitude: 50.45466,
  longitude: 30.5238
};

export const GMAP_API_URL = 'https://www.googleapis.com/fusiontables/v2/query';
export const GMAP_API_KEY = 'AIzaSyC7xiy02ErqtTm0R4I774YW7hiKI6RtpMc';
export const GMAP_API_SELECT = 'markers';
export const GMAP_API_FROM = '1-xUGejfo6LBMS7aXgJ-Ohp6PSRutZaEG2HfoOSth';


export const checkMosstCodeStatus = {
  created: (val) => val === 50,
  formed: (val) => val === 83,
  scanned: (val) => val === 86 || val === 87,
  processingQueue: (val) => val === 149,
  processed: (val) => val === 150,
  expired: (val) => val === 249,
  removed: (val) => val === 250
}