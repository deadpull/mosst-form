import { Component, OnInit, EventEmitter, Output, Input, HostListener, ElementRef, ViewChild } from '@angular/core';
import { FormControl, AbstractControl, Validators } from '@angular/forms';

import { Observable } from "rxjs";
import { RegistrationService } from "../registration.service";
import { IOTPCheckResult } from '../interfaces';

import constants from '../constants';

@Component({
  selector: 'app-forgot-pass-form',
  template: `
    <div class="reg-form-wrapper reg-otp-form">
      <div class="reg-register-form__img-holder">
        <div class="reg-icon-img"><img src="../assets/images/phone.svg" alt=""></div>
        <div class="reg-main-img"><img src="../assets/images/register-pass.png" alt=""></div>
        <div class="reg-icon-img"><img src="../assets/images/lock-plus.svg" alt=""></div>
      </div>
      <div class="reg-actions-block__sms-confirm">
        <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': wrongOTP }">
          <input id="sms-password"
                 [formControl]="OTPCodeInput"
                 [maxlength]="otpMaxLength"
                 autocomplete="off"
                 autofocus
                 required
                 [type]="showPass ? 'text' : 'password'"
                 #autofocusInput
          >
          <label for="sms-password">{{'sms_password' | translate}}</label>
          <span class="reg-pass-icon" (click)="togglePassShow()">
        <img [src]="showPass ? '../assets/images/eye.svg' : '../assets/images/eye-off.svg'" alt="">
      </span>
          <div class="reg-error-field" *ngIf="wrongOTP && verification_attempts !== 0"> {{'sms_password_wrong' | translate}}</div>
          <div class="reg-error-field" *ngIf="verification_attempts === 0">
            {{'verification_attempts_zero' | translate}}
          </div>
        </div>
        <div class="reg-form-info">{{'enter_sms_password' | translate}}</div>
        <div class="reg-attempts">{{attempts}}</div>
        <button class="reg-button"  (click)="sendOtp()" [disabled]="!OTPCodeInput.valid">{{'confirm' | translate}}</button>

      </div>
    </div>
  `
})
export class ForgotFormComponent implements OnInit {
  @ViewChild('autofocusInput')
  public autofocusInputRef: ElementRef;

  public otpMaxLength: number = constants.otpMaxLength;

  public showPass: boolean = false;
  public wrongOTP: boolean = false;
  public verification_attempts: number;

  public OTPCodeInput: AbstractControl;

  @Input()
  verification_guid: string;

  @Output()
  otpVerified: EventEmitter<boolean> = new EventEmitter<boolean>();

  @HostListener('window:keydown', ['$event'])
  keyboardInput(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      if (this.OTPCodeInput.valid) {
        this.sendOtp();
      }
    }
  }

  constructor(private service: RegistrationService) {
    this.OTPCodeInput = new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]);
    this.OTPCodeInput.valueChanges.subscribe(this.OTPChange.bind(this));
  }

  OTPChange() {
    this.wrongOTP = false;
  }

  togglePassShow() {
    this.showPass = !this.showPass;
  }

  get attempts() {
    let attemptsText = '';
    switch (this.verification_attempts) {
      case 1:
        attemptsText = 'Осталась 1 попытка';
        break;
      case 2:
        attemptsText = 'Осталось 2 попытки';
        break;
      case 3:
        attemptsText = 'Осталось 3 попытки';
        break;
      default:
        break;
    }
    return attemptsText;
  }

  sendOtp() {
    if (this.OTPCodeInput.valid && this.verification_attempts !== 0) {
      const otpCheck$: Observable<IOTPCheckResult> = this.service.checkOTPCode(this.OTPCodeInput.value, this.verification_guid);
      otpCheck$.subscribe((results: IOTPCheckResult) => {
        if (results.status === 'Y') {
          this.otpVerified.emit(true);
          this.wrongOTP = false;
        } else {
          this.otpVerified.emit(false);
          this.wrongOTP = true;
        }
        this.verification_attempts = results.verification_attempts;
      })
    }
  }

  ngOnInit() {
    setTimeout(() => this.autofocusInputRef.nativeElement.focus(), 100);
  }
}
