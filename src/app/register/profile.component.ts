import {Component, Input, OnInit, OnChanges, Output, EventEmitter} from '@angular/core';

import { RegistrationService } from './registration.service';
import { IUser, IServiceConfig } from './interfaces';
import { LocaleService } from './locale.service';
import { ILocales } from './interfaces';
import { MaskWorker } from './mask.worker';

export interface IProfileOptions {
  buttons?: {
    editAvatar?: boolean;
    editInfo?: boolean;
    editPassword?: boolean;
  };
}

@Component({
  selector: 'mosst-profile',
  template: `
    <div class="reg-register-form" *ngIf="user">
      <div class="reg-register-form__title">{{'profile' | translate}}</div>
      <div class="reg-register-form__actions-holder profile-view-content">
        <div *ngIf="!state" class="reg-register-form__actions-holder">
          <div>
            <div class="reg-form-wrapper reg-phone-form">
              <div class="reg-register-form__img-holder">
                <div class="reg-main-img">
                  <app-avatar
                    [src]="avatarSrc"
                    [error]="avatarError"
                    [editable]="!options?.buttons || options?.buttons?.editAvatar !== false"
                    (onChange)="onAvatarChange($event)"
                  ></app-avatar>
                </div>
              </div>
      
              <h3>{{user.first_name}} {{user.last_name}}</h3>
              <h4 phone>{{msisdn}}</h4>
              <br>
      
              <div>
                <button *ngIf="!options?.buttons || options?.buttons?.editInfo !== false"
                  class="profile-view-button change-pass"
                  type="button"
                  (click)="edit()"
                >
                  <i class="mdi mdi-account"></i>
                  {{'personal_info' | translate}}
                </button>
      
                <button *ngIf="!options?.buttons || options?.buttons?.editPassword !== false"
                  class="profile-view-button change-pass"
                  type="button"
                  (click)="changePassword()"
                >
                  <i class="mdi mdi-lock"></i>
                  {{'change_password' | translate}}
                </button>
      
                <button class="profile-view-button" type="button" (click)="logout()">
                  <i class="mdi mdi-exit-to-app"></i>
                  {{'logout' | translate}}
                </button>
              </div>
            </div>
          </div>

          <div>
            <button class="profile-close-button" (click)="close()">{{'cancel' | translate}}</button>
          </div>
        </div>

        <div *ngIf="state === 'changePassword'">
          <app-change-password-form
            [wrongPassword]="wrongPassword"
            (onSubmit)="editProfile($event)"
            (onCancel)="state = ''"
          ></app-change-password-form>
        </div>

        <div *ngIf="state === 'edit'">
          <app-edit-profile-form
            [user]="user"
            (onSubmit)="editProfile($event)"
            (onCancel)="state = ''"
          ></app-edit-profile-form>
        </div>
      </div>
    </div>
  `
})
export class ProfileComponent implements OnInit, OnChanges {
  @Input()
  public user: IUser;

  @Input()
  public lang: string;

  @Input()
  public config: IServiceConfig;

  @Input()
  public locales: ILocales = {};

  @Input()
  public options: IProfileOptions = {
    buttons: {
      editAvatar: true,
      editInfo: true,
      editPassword: true
    }
  };

  @Output()
  public onLogout: EventEmitter<void>;

  @Output()
  public onClose: EventEmitter<void>;

  @Output()
  public onUserUpdate: EventEmitter<IUser>;

  public hidden: boolean = false;

  public state: '' | 'changePassword' | 'edit';

  public msisdn: string;

  public avatarSrc: string = '';

  public wrongPassword: boolean = false;

  public avatarError: string = '';

  public constructor(
    private service: RegistrationService,
    private localeService: LocaleService
  ) {
    this.onLogout = new EventEmitter<void>();
    this.onClose = new EventEmitter<void>();
    this.onUserUpdate = new EventEmitter<IUser>();
  }

  public ngOnInit(): void {
    this.localeService.setLocale(this.lang);
    this.localeService.updateLocales(this.locales);

    setTimeout(() => {
      this.service.config = this.config;
      this.formatPhone();
    });
  }

  public ngOnChanges(changes): void {
    this.localeService.setLocale(this.lang);

    setTimeout(() => {
      this.service.config = this.config;
      this.avatarSrc = this.service.getUserAvatar(this.user);
      this.formatPhone();
    });
  }

  public changePassword(): void {
    this.state = 'changePassword';
  }

  public edit(): void {
    this.state = 'edit';
  }

  public logout(): void {
    this.user = null;
    this.onLogout.emit();
  }

  public close(): void {
    this.state = '';
    this.hidden = true;
    this.onClose.emit();
  }

  public editProfile(data): void {
    this.service.editUser(this.user, data)
      .subscribe(
        (updatedData) => {
          if (!updatedData) {
            return;
          }

          this.onUserUpdate.emit(updatedData);
          this.state = '';
        },
        (res) => {
          if (data.photo && res.status === 413)  {
            this.showAvatarSizeError();
          } else {
            this.wrongPassword = true;
          }
        }
      );
  }

  public formatPhone(): void {
    if (!this.user) {
      return;
    }

    const {msisdn} = this.user;
    const _value: string = msisdn.slice(0, 8) + '***' + msisdn.slice(-2);
    const maskWorker = new MaskWorker('+38 (0XX) XXX XX XX', '+380XXXXXXXXX', '[0-9|\*]');
    this.msisdn = maskWorker.useMask(_value);
  }

  public onAvatarChange(data): void {

    if (!data) {
      this.showAvatarSizeError();
      return;
    }

    this.avatarError = '';
    this.editProfile({photo: data});
  }

  private showAvatarSizeError(): void {
    this.avatarError = 'avatar_size_too_large';
    setTimeout(() => this.avatarError = '', 2 * 1000);
  }
}
