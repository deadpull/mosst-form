export default  {
  passwordMinLength: 6,
  passwordMaxLength: 16,
  passwordRegexp: '^[a-zA-Z0-9]*$',
  otpMaxLength: 6
};
