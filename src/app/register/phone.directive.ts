import { Directive, EventEmitter, Input, Output, OnInit, ElementRef } from '@angular/core';
import { NgModel, AbstractControl } from "@angular/forms";
import { MaskWorker } from "./mask.worker";

function getFinalSelectionPosition(pos) {
  if (pos <= 6) {
    return 7;
  }
  let defaultPosition = pos;

  switch (pos) {
    case 9:
      defaultPosition = defaultPosition + 2;
      break;
    case 14:
      defaultPosition++;
      break;
    case 17:
      defaultPosition++;
      break;
  }

  return defaultPosition;
}

export function formatPhone(value = '') {
  const maskWorker = new MaskWorker('+38 (0XX) XXX XX XX', '+380XXXXXXXXX', '[0-9]');
  return maskWorker.useMask(value);
}

@Directive({
  selector: '[phone]',
  providers: [NgModel],
  host: {
    '(ngModelChange)': 'onInputChange($event)'
  }
})
export class PhoneDirective implements OnInit {

  private maskWorker: MaskWorker;

  @Input() formControl: AbstractControl;

  @Output() valueChanges: EventEmitter<string> = new EventEmitter<string>();

  constructor(private model: NgModel, private el: ElementRef) {
    this.maskWorker = new MaskWorker('+38 (0XX) XXX XX XX', '+380XXXXXXXXX', '[0-9]', function (string) {
      // на случай, если пользователь пытается ввести телефон начиная с символов (3\8\0), которые разрешены маской,
      // но низя
      if (string.length === 7) {
        if (/[0]/.test(string[6])) {
          return string.slice(0, 6);
        }
      }
      return string;
    });
    this.valueChanges.subscribe(phone => {
      if (this.formControl) {
        this.formControl.setValue(phone, {
          onlySelf: true,
          emitEvent: false,
          emitModelToViewChange: false,
          emitViewToModelChange: false
        });
      }
    });
  }

  ngOnInit() {
    if (this.formControl) {
      this.formControl.setValue(this.maskWorker.useMask(this.formControl.value || ''));
    }
  }

  onInputChange(event) {
    const start = this.el.nativeElement.selectionStart;

    const maskedValue = this.maskWorker.useMask(event);
    this.model.valueAccessor.writeValue(maskedValue);

    this.el.nativeElement.selectionStart = getFinalSelectionPosition(start);
    this.el.nativeElement.selectionEnd = getFinalSelectionPosition(start);

    this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
  }

}
