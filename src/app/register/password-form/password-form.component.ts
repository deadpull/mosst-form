import {Component, EventEmitter, Output, Input, HostListener, ViewChild, ElementRef, OnInit} from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {RegistrationService} from '../registration.service';
import constants from '../constants';
import {ErrorService} from '../error.service';

@Component({
  selector: 'app-password-form',
  template: `
    <div class="reg-form-wrapper reg-password-form">
      <div class="reg-register-form__img-holder">
        <div class="reg-icon-img"><!--<img src="assets/images/phone.svg" alt="">--></div>
        <div class="reg-icon-img"><!--&gt;<img src="assets/images/message.svg" alt="">--></div>
        <div class="reg-main-img"><img src="assets/images/blue-lock.svg" alt=""></div>
      </div>
      <div class="reg-change-pass" [formGroup]="passwords">
        <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': !formValid()}">
          <input id="new-password" formControlName="newPassword" autofocus #autofocusInput [type]="showPassword ? 'text' : 'password'"
                 required>
          <label for="new-password">{{'set_password' | translate}}</label>
          <span class="reg-pass-icon" (click)="toggleShow('password')">
        <img [src]="showPassword ? '../assets/images/eye.svg' : '../assets/images/eye-off.svg'" alt="">
        </span>
        </div>
        <div class="reg-error-field" *ngIf="!formValid()">
          {{getErrorText('newPassword') | translate: getErrorParams('newPassword')}}
        </div>
        <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': !formValid()}">
          <input id="repeat-password" formControlName="repeat" [type]="showConfirm ? 'text' : 'password'" required>
          <label for="repeat-password">{{'repeat_password' | translate}}</label>
          <span class="reg-pass-icon" (click)="toggleShow('confirm')">
        <img [src]="showConfirm ? '../assets/images/eye.svg' : '../assets/images/eye-off.svg'" alt="">
      </span>
        </div>
        <div class="reg-error-field" *ngIf="!formValid()">
          {{getErrorText('repeat') | translate: getErrorParams('repeat')}}
        </div>
        <div class="reg-error-field" *ngIf="!formValid()">
          {{'password_mismatch' | translate}}
        </div>
        <div class="reg-form-info">{{'set_password_using_symbols' | translate}}</div>
      </div>
    </div>
    <div class="controls">
      <button *ngIf="!registrationSuccessEnd" class="reg-button" [disabled]="!password" (click)="registerButtonClick()">{{'proceed' | translate}}</button>
      <div *ngIf="registrationSuccessEnd" class="success-stats">
        <img src="../assets/images/check-white.svg" alt="	✓">
      </div>
    </div>
  `,
})
export class PasswordFormComponent implements OnInit {
  @ViewChild('autofocusInput')
  public autofocusInputRef: ElementRef;

  public showPassword = false;
  public showConfirm = false;
  public passwords: FormGroup;
  public registrationSuccessEnd: boolean = false;

  private password: string;

  @Input()
  verification_guid: string;

  @Input()
  msisdn: string;

  @Output()
  onClose: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  registrationSuccess: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  forgotPassEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @HostListener('window:keydown', ['$event'])
  keyboardInput(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      if (this.passwords.valid) {
        this.registerButtonClick();
      }
    }
  }

  constructor(private service: RegistrationService, private errorService: ErrorService) {
    const validators = [Validators.required,
      Validators.minLength(constants.passwordMinLength),
      Validators.maxLength(constants.passwordMaxLength),
      Validators.pattern(constants.passwordRegexp)];

    this.passwords = new FormGroup({
      newPassword: new FormControl('', validators),
      repeat: new FormControl('', validators)
    }, this.areEqual);
    this.passwords.valueChanges.subscribe(this.passwordFormChange.bind(this));
  }

  ngOnInit() {
    setTimeout(() => this.autofocusInputRef.nativeElement.focus(), 100);
  }

  formControlValid(control: FormControl) {
    return !control.touched || !control.dirty
  }

  closeClicked() {
    this.onClose.emit();
  }

  formValid() {
    return this.passwords.valid
      || this.formControlValid(<FormControl>this.passwords.controls['newPassword'])
      || this.formControlValid(<FormControl>this.passwords.controls['repeat']);
  }

  passwordFormChange() {
    this.password = '';

    if (this.passwords.valid) {

      this.password = this.passwords.controls['repeat'].value;
    }
  }

  registerButtonClick() {
    if (this.forgotPassEvent) {
      const user$: Observable<any> = this.service.registerNewPassForgot(this.password, this.msisdn, this.verification_guid);
      user$.subscribe((userRegistered: boolean) => {
        this.registrationSuccess.emit(this.password);
        this.registrationSuccessEnd = true
      });
    } else {
      const user$: Observable<any> = this.service.register(this.password, this.msisdn, this.verification_guid);
      user$.subscribe((userRegistered: boolean) => {
        this.registrationSuccess.emit(this.password);
        this.registrationSuccessEnd = true
      });
    }
  }

  getErrorText(formControlName) {
    const errorObject = this.errorService.getError(this.passwords.controls[formControlName], 'password');
    return errorObject && errorObject.text || '';
  }

  getErrorParams(formControlName) {
    const errorObject = this.errorService.getError(this.passwords.controls[formControlName], 'password')
    return errorObject && errorObject.params || '';
  }

  toggleShow(field) {
    if (field === 'confirm') {
      this.showConfirm = !this.showConfirm;
    } else if (field === 'password') {
      this.showPassword = !this.showPassword;
    }
  }

  areEqual(group: FormGroup) {
    let valid;

    let currentValue;

    for (const name in group.controls) {
      if (group.controls.hasOwnProperty(name)) {
        if (!currentValue) {
          currentValue = group.controls[name].value;
        }

        valid = currentValue === group.controls[name].value;
      }
    }

    if (valid) {
      return null;
    }

    return {
      areEqual: true
    };
  }
}
