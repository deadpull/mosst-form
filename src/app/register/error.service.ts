import { Injectable } from '@angular/core';
import { FormControl, AbstractControl } from "@angular/forms";
import constants from './constants';

const errorTranslateKeys = {
  password: {
    minlength: {
      text: 'error_min_length',
      params: constants.passwordMinLength
    },
    maxlength: {
      text: 'error_max_length',
      params: constants.passwordMaxLength
    },
    pattern: {
      text: 'error_wrong_symbols'
    }
  }
};

@Injectable()
export class ErrorService {

  public getError(formControl: AbstractControl, type: string) {
    let errorFound = false;
    let currentError = '';
    for (let key in formControl.errors || {}) {
      if (!errorFound) {
        currentError = key;
        errorFound = true;
      }
    }
    return this.getErrorTextKey(currentError, type);
  }


  getErrorTextKey(error: string, type: string) {
    if (!error) {
      return null;
    }

    return errorTranslateKeys[type][error];
  }
}