import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { RegistrationComponent } from './registration.component';
import { ProfileComponent } from './profile.component';

import { OtpFormComponent } from './otp-form/otp-form.component';
import { PasswordFormComponent } from './password-form/password-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ForgotFormComponent } from './forgot-pass-form/forgot-pass-form.component';
import { PhoneFormComponent } from "./phone-form/phone-form.component";
import { ChangePasswordFormComponent } from './change-password-form/change-password-form.component';
import { EditProfileForm } from './edit-profile-form/edit-profile-form.component';

import { AvatarComponent } from './avatar/avatar.component';

import { PhoneDirective } from './phone.directive';

import { RegistrationService } from './registration.service';
import { LocaleService } from './locale.service';

import { TranslatePipe } from './pipes/translate';
import { ErrorService } from "./error.service";

@NgModule({
  declarations: [
    PhoneDirective,
    RegistrationComponent,
    ProfileComponent,
    OtpFormComponent,
    ForgotFormComponent,
    PasswordFormComponent,
    LoginFormComponent,
    PhoneFormComponent,
    ChangePasswordFormComponent,
    EditProfileForm,
    AvatarComponent,
    TranslatePipe
  ],
  providers: [
    ErrorService,
    RegistrationService,
    LocaleService
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    HttpModule
  ],
  exports: [
    RegistrationComponent,
    ProfileComponent
  ]
})
export class RegistrationModule {}
