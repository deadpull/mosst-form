import {Component, OnInit, Input, Output, EventEmitter, HostListener, ViewChild, ElementRef} from '@angular/core';
import {FormControl, AbstractControl, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

import {RegistrationService} from '../registration.service';
import {IUser, IUserCheckResult} from '../interfaces';

@Component({
  selector: 'app-login-form',
  template: `
    <div class="reg-form-wrapper reg-login-form">
      <div class="reg-register-form__img-holder">
        <div class="reg-main-img"><img src="../assets/images/blue-user.svg" alt=""></div>
        <div class="reg-icon-img"><!--<img src="../assets/images/phone.svg" alt="">--></div>
      </div>
      <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': wrongPassword}">
        <input
          id="password"
          [formControl]="passwordField"
          autofocus
          [type]="showPass ? 'text' : 'password'"
          (change)="passwordInputChange()"
          (keyup)="passwordInputKeyUp($event)"
          required
          #autofocusInput
        >
        <label for="password">{{'password' | translate}}</label>
        <span class="reg-pass-icon" (click)="togglePassShow()">
        <img [src]="showPass ? '../assets/images/eye.svg' : '../assets/images/eye-off.svg'" alt="">
    </span>
        <div class="reg-error-field" *ngIf="wrongPassword">
          {{'password_wrong' | translate}}
        </div>
      </div>
      <div class="reg-form-info">
        <span>{{'enter_password_request' | translate }}</span>
        <button type="button" class="reg-form-forgot" (click)="forgotPass()">{{'forgot_password_request' | translate }}</button>
      </div>
    </div>
    <div class="controls">
      <button *ngIf="!registrationSuccess" class="reg-button" [disabled]="!passwordField.value" (click)="loginButtonClick()">
        {{'login' | translate }}
      </button>
      <div *ngIf="registrationSuccess" class="success-stats">
        <img src="../assets/images/check-white.svg" alt="	✓">
      </div>
    </div>
  `,
})
export class LoginFormComponent implements OnInit {
  @ViewChild('autofocusInput')
  public autofocusInputRef: ElementRef;

  public passwordField: AbstractControl;
  public wrongPassword = false;
  public showPass: boolean = false;
  public registrationSuccess: boolean = false;
  
  private password: string;

  @Input() msisdn: string;

  @Input()
  verification_guid: string;

  @Output()
  user: EventEmitter<IUser> = new EventEmitter<IUser>()

  @Output()
  forgotPassEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  userCredentials: EventEmitter<IUserCheckResult> = new EventEmitter<IUserCheckResult>();

  @HostListener('window:keydown', ['$event'])

  keyboardInput(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      this.loginButtonClick();
    }
  }

  constructor(private service: RegistrationService) {
  }

  ngOnInit() {
    this.passwordField = new FormControl('', [Validators.required, Validators.minLength(3)]);
    setTimeout(() => this.autofocusInputRef.nativeElement.focus(), 100);
  }

  togglePassShow() {
    this.showPass = !this.showPass;
  }

  loginButtonClick() {
    if (!this.passwordField.valid) {
      this.wrongPassword = true;
      return;
    }

    this.service.login(this.passwordField.value, this.msisdn)
      .subscribe(data => {
        if (!data.wrongPassword) {
          this.registrationSuccess = true
          return this.user.emit(data)
        } else {
          this.wrongPassword = data.wrongPassword;
        }
      });
  }

  forgotPass() {
    this.forgotPassEvent.emit(true)
  }

  passwordInputKeyUp(event) {
    if (event.keyCode !== 13) {
      this.wrongPassword = false;
    }
  }

  passwordInputChange() {
    this.password = '';
    if (this.passwordField.valid) {
      this.password = this.passwordField.value;
    }
  }

}
