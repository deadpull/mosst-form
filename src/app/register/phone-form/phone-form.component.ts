import {Component, HostListener, EventEmitter, Output, Input, ViewChild, ElementRef, OnInit} from '@angular/core';
import {FormControl, Validators, AbstractControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {RegistrationService} from '../registration.service';
import {IUserCheckResult} from '../interfaces';

@Component({
  selector: 'app-phone-form',
  template: `
    <div class="reg-form-wrapper reg-phone-form">
      <div class="reg-register-form__img-holder">
        <div class="reg-main-img"><img src="assets/images/blue-logo.svg" alt=""></div>
        <div *ngIf="action==='login' && !(userCheckDone && !userExists)" class="reg-icon-img lock-state">
          <!--<img src="assets/images/lock.svg" alt="">-->
        </div>
        <div *ngIf="action==='register' || (action==='login' && (userCheckDone && !userExists))" class="reg-icon-img">
          <img src="assets/images/message.svg" alt=""></div>
        <div *ngIf="action==='register' || (action==='login' && (userCheckDone && !userExists))" class="reg-icon-img">
          <!--<img src="assets/images/lock-plus.svg" alt="">--></div>
      </div>
      <div class="reg-input-holder reg-simple-field reg-phone-field" [ngClass]="{'error-input': showError}">
        <input id="phone"
               type="tel"
               phone
               (valueChanges)="valueChange()"
               [formControl]="phoneInput"
               required
               #autofocusInput
        >
        <label for="phone">{{'phone_number' | translate}}</label>
        <span *ngIf="userExists" class="reg-pass-icon">
        <img src="../assets/images/done.svg" alt="">
      </span>
        <div class="reg-error-field" *ngIf="showError">
          {{'wong_phone_number' | translate}}
        </div>
      </div>
      <div *ngIf="action==='register' && !userExists" class="reg-form-info">
        {{'phone_not_registered_hint' | translate}}
      </div>
      <div class="reg-form-info icon-on-side">
       <img src="assets/images/sm-lock.svg"> <span>{{'phone_registered_hint1' | translate}}</span>
      </div>
      <div class="reg-form-info icon-on-side">
       <img src="assets/images/add-user.svg"> <span>{{'phone_registered_hint2' | translate}}</span>
      </div>
      <div *ngIf="userExists && action==='register'" class="reg-form-info">
        {{'phone_already_registered' | translate }}
      </div>
      <div class="controls">
        <button class="reg-button" [disabled]="!userData" (click)="userExists ? confirmUser(): sendOtp()">
          {{userExists && action === 'register' ? ('enter_password' | translate) : ('proceed' | translate)}}
        </button>
      </div>
    </div>
  `,
})
export class PhoneFormComponent implements OnInit {
  @ViewChild('autofocusInput')
  public autofocusInputRef: ElementRef;

  @Input()
  action: string;

  @Input()
  lang: string;

  @Output()
  phone: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  userCredentials: EventEmitter<IUserCheckResult> = new EventEmitter<IUserCheckResult>();

  @Output()
  userExistsEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @HostListener('window:keydown', ['$event'])
  keyboardInput(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      if (this.userData) {
        this.userExists ? this.confirmUser() : this.sendOtp();
      }
    }
  }

  public userCheckDone = false;
  private _userExists;

  public get userExists() {
    return this._userExists;
  }

  public set userExists(value) {
    this._userExists = value;
    this.userExistsEvent.emit(value)
  };

  public showError = false;
  public userData: IUserCheckResult = null;

  public phoneInput: AbstractControl;
  private latestValue = '';

  constructor(private service: RegistrationService) {

    this.userExists = false;
    this.phoneInput = new FormControl('', [Validators.required,
      Validators.pattern(/^\+380(39|50|63|66|67|68|91|92|93|94|95|96|97|98|99)\d{7}/)]);
  }

  ngOnInit() {
    setTimeout(() => this.autofocusInputRef.nativeElement.focus(), 100);
  }

  sendOtp() {
    if (this.userData) {
      this.userCredentials.emit(this.userData);
    }
  }

  enableErrorField() {
    if (this.phoneInput.value && this.phoneInput.value.length === 13) {
      return !this.phoneInput.valid;
    }
    return false;
  }

  valueChange() {
    if (this.latestValue === this.phoneInput.value) {
      return;
    }
    this.latestValue = this.phoneInput.value;
    this.userCheckDone = false;
    this.userData = null;
    this.userExists = false;
    this.phone.emit('');

    this.showError = false;

    if (this.phoneInput.value && this.phoneInput.value.length === 13) {
      this.showError = !this.phoneInput.valid;
    }

    if (this.phoneInput.valid) {
      this.phone.emit(this.phoneInput.value);
      this.checkUserExists(this.phoneInput.value);
    }
  }

  checkUserExists(number: string) {
    const userCheckResults$: Observable<IUserCheckResult> = this.service.getUserAuthInfo(number);
    userCheckResults$.subscribe((results: IUserCheckResult) => {
      this.userCheckDone = true;
      if (!results) {
        this.userExists = false;
        this.userData = null;
        return;
      }
      this.userExists = results.userExists;
      this.userData = results;
    });
  }

  confirmUser() {
    this.userCredentials.emit(this.userData);
  }
}
