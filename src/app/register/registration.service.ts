import { Injectable } from '@angular/core'
import { Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import {
  IUser,
  IServiceConfig,
  IUserCheckResult,
  IOTPCheckResult
} from './interfaces';

const userExistsMessage = 'profile exists';

@Injectable()
export class RegistrationService {
  public config: IServiceConfig;

  public constructor(private http: Http) {}

  public getUserAuthInfo(phoneNumber: string): Observable<IUserCheckResult> {
    const url = this.config.registrationUrl;

    const method = 'POST';
    const body = {msisdn: phoneNumber, web: 1};
    const options = new RequestOptions({url, method, body});

    return this.http.request(url, options)
      .map((res: Response): any  => {
        const result: any  = {userExists: false};

        if (res && res.status === 200) {
          const body = res.json();

          if (body.error) {
            if (body.error === userExistsMessage) {
              result.userExists = true;
              return result;
            }
            return null;
          }

          result.verification_guid = body.verification.verification_guid;
          result.verification_attempts = body.verification.verification_attempts;
        }
        return result;
      }).catch((error: any, caught: Observable<IUserCheckResult>) => {
        return Observable.of({});
      });
  }

  public forgotPassGetUser(phoneNumber: string): Observable<IUserCheckResult> {
    const url = this.config.forgotUrl;

    const method = 'POST';
    const body = {msisdn: phoneNumber, web: 1};
    const options = new RequestOptions({url, method, body});

    return this.http.request(url, options)
      .map((res: Response): any  => {
        const result: any  = {userExists: false};

        if (res && res.status === 200) {
          const body = res.json();

          if (body.error) {
            if (body.error === userExistsMessage) {
              result.userExists = true;
              return result;
            }
            return null;
          }

          result.verification_guid = body.verification.verification_guid;
          result.verification_attempts = body.verification.verification_attempts;
        }
        return result;
      }).catch((error: any, caught: Observable<IUserCheckResult>) => {
        return Observable.of({});
      });
  }

  public checkOTPCode(otpCode: string, verification_guid: string): Observable<IOTPCheckResult> {
    const url = this.config.otpCheckUrl;
    const method = 'POST';
    const body = {vvalue: otpCode, verification_guid: verification_guid};
    const options = new RequestOptions({url, method, body});
    return this.http.request(url, options)
      .map((res: Response) => {
        const result: IOTPCheckResult = {};
        if (res && res.status === 200) {
          const body = res.json();

          if (body.error) {
            return result;
          }

          if (body.verification) {
            result.status = body.verification.status;
            result.verification_attempts = body.verification.verification_attempts;
          }
        }
        return result;
      });
  }

  public login(password: string, msisdn: string) : Observable<any>{
    const url = this.config.loginUrl;
    const method = 'POST';
    const body = {msisdn, password};
    const options = new RequestOptions({url, method, body});
    return this.http.request(url, options)
      .map((res: Response) => {
        let result: any = {};
        if (res && res.status === 200) {
          const body = res.json();

          if (body.error) {
            throw new Error(body.error);
          }

          result = body;
        }

        return result;
      })
      .flatMap((user) => this.getProfile(user)
        .map((profile) => Object.assign({}, user, profile))
      )
      .catch((err) => {
        return Observable.of({wrongPassword: true});
      });
  }

  public registerNewPassForgot(password: string, msisdn: string, verification_guid?: string) {
    const url = this.config.forgotUrl;
    const method = 'POST';
    const body = {msisdn, password, verification_guid, web: 1};
    const options = new RequestOptions({url, method, body});
    return this.http.request(url, options)
      .map((res: Response): boolean => {
        if (res && res.status === 200) {
          const body = res.json();

          if (body && body.profile == 'created') {
            return true;
          }

          if (body.error) {
            return false;
          }
        }
        return false;
      });
  }

  public register(password: string, msisdn: string, verification_guid?: string) {
    const url = this.config.registrationUrl;
    const method = 'POST';
    const body = {msisdn, password, verification_guid, web: 1};
    const options = new RequestOptions({url, method, body});
    return this.http.request(url, options)
      .map((res: Response): boolean => {
        if (res && res.status === 200) {
          const body = res.json();

          if (body && body.profile == 'created') {
            return true;
          }

          if (body.error) {
            return false;
          }
        }
        return false;
      });
  }

  public editUser(user: IUser, updatedata: any): Observable<any> {
    const {profile_session_guid, profile_guid} = user;

    const url = this.config.profileUrl;
    const method = 'POST';
    const body = {profile_session_guid, profile_guid, updatedata};
    const options = new RequestOptions({url, method, body});

    return this.http.request(url, options)
      .map((res: Response) => {
        if (res && res.status === 200) {
          const json = res.json();

          if (!json.error) {
            return Object.assign({}, json.profile, {
              avatar_path: json.profile.avatar_guid
            });
          } else {
            throw new Error(json.message || json.error);
          }
        }

        return null;
      });
  }

  public getProfile(user: IUser): Observable<any> {
    const {profile_session_guid, profile_guid} = user;

    const url = this.config.profileUrl;
    const method = 'POST';
    const body = {profile_session_guid, profile_guid};
    const options = new RequestOptions({url, method, body});

    return this.http.request(url, options)
      .map((res: Response) => {
        if (res && res.status === 200) {
          const json = res.json();

          if (!json.error) {
            return Object.assign({}, json.profile, json.session, {
              avatar_path: json.profile.avatar_guid
            });
          } else {
            throw new Error(json.message);
          }
        }

        return null;
      });
  }

  public getUserAvatar(user: IUser, size: number = 200): string {
    const {avatarUrl} = this.config;

    if (!avatarUrl) {
      return null;
    }

    const {avatar_path} = user;

    return avatar_path ? `${avatarUrl}/${avatar_path}_${size}.jpg` : '';
  }
}
