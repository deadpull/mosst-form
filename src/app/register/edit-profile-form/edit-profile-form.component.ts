import { Component, Input, Output, EventEmitter, OnInit, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, Validator } from '@angular/forms';

import { IUser } from '../interfaces';

@Component({
  selector: 'app-edit-profile-form',
  template: `
    <form class="reg-form-wrapper" [formGroup]="formGroup" (ngSubmit)="submit()">
      <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': !validateControl('firstName')}">
        <input
          id="firstName"
          autocomplete="off"
          required
          type="text"
          formControlName="firstName"
        >
        <label for="firstName">{{'first_name' | translate}}</label>
      </div>

      <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': !validateControl('lastName')}">
        <input
          id="lastName"
          autocomplete="off"
          autofocus
          required
          type="text"
          formControlName="lastName"
          #autofocusInput
        >
        <label for="lastName">{{'last_name' | translate}}</label>
      </div>

      <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': !validateControl('middleName')}">
        <input
          id="middleName"
          autocomplete="off"
          type="text"
          formControlName="middleName"
        >
        <label for="middleName">{{'middle_name' | translate}}</label>
      </div>

      <div class="reg-input-holder reg-simple-field" [ngClass]="{'error-input': !validateControl('email')}">
        <input
          id="email"
          autocomplete="off"
          type="text"
          formControlName="email"
        >
        <label for="email">{{'email' | translate}}</label>
      </div>
      
      <div class="button-block">
        <button type="button" class="cancel--static" (click)="close()">{{'cancel' | translate}}</button>
        <button type="submit" class="reg-button" [disabled]="!formGroup.valid">{{'confirm' | translate}}</button>
      </div>
    </form>
  `
})
export class EditProfileForm implements OnInit, OnChanges {
  @ViewChild('autofocusInput')
  public autofocusInputRef: ElementRef;

  @Input()
  public user: IUser;

  @Output()
  public onSubmit: EventEmitter<any>;

  @Output()
  public onCancel: EventEmitter<any>;

  public formGroup: FormGroup;

  public constructor() {
    this.onSubmit = new EventEmitter<any>();
    this.onCancel = new EventEmitter<any>();

    this.formGroup = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      middleName: new FormControl(''),
      email: new FormControl('', Validators.pattern(/[a-zA-Z0-9_-]+@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)?/))
  });
  }

  public ngOnInit(): void {
    setTimeout(() => this.initFormValues());
    setTimeout(() => this.autofocusInputRef.nativeElement.focus(), 100);
  }

  public ngOnChanges(): void {
    setTimeout(() => this.initFormValues());
  }

  public initFormValues(): void {
    this.formGroup.setValue({
      firstName: this.user ? this.user.first_name || '' : '',
      lastName: this.user ? this.user.last_name || '' : '',
      middleName: this.user ? this.user.middle_name || '' : '',
      email: this.user && this.user.emails ? this.user.emails[0] || '' : ''
    });
  }

  public validateControl(key: string): boolean {
    const control = this.formGroup.controls[key];
    return !control || !!(control.valid || !control.touched);
  }

  public submit(): void {
    if (!this.formGroup.valid) {
      return;
    }

    const {firstName, lastName, middleName, email} = this.formGroup.value;
    this.onSubmit.emit({
      first_name: firstName,
      last_name: lastName,
      middle_name: middleName || '',
      email
    });
  }

  public close(): void {
    this.onCancel.emit();
  }
}
