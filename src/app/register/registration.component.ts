import {Component, Input, OnInit, OnChanges, Output, EventEmitter} from '@angular/core';
import {Observable} from "rxjs";
import {RegistrationService} from "./registration.service";
import {ILocales, IUser, IServiceConfig, IUserCheckResult} from './interfaces';
import {LocaleService} from './locale.service';

export interface IExtraOptions {
  userCheckDone?: boolean
  registrationSuccess?: boolean
  userExists?: boolean
  otpVerified?: boolean;
  user_guie?: string;
  forgotPassword?: boolean;
}

@Component({
  selector: 'registration',
  template: `
    <div class="reg-register-form">
      <div class="reg-register-form__title">{{getActionTitle() | translate}} {{systemName}}</div>
      <!--<span class="reg-register-form__close" (click)="closeClicked()"></span>-->

      <div [ngSwitch]="getAction()" class="reg-register-form__actions-holder">

        <div *ngSwitchCase="actions.none">
          <app-phone-form
            [lang]="lang"
            [action]="action"
            (userExistsEvent)="setUserAction($event)"
            (userCredentials)="getUserCredentials($event)"
            (phone)="getMsisdn($event)" class="form-holder">
          </app-phone-form>
        </div>

        <div *ngSwitchCase="actions.login">
          <app-login-form class="form-holder" [msisdn]="msisdn"
                          (user)="getUser($event)"
                          (forgotPassEvent)="forgotPassWord($event)"
                          [verification_guid]="verificationGuid">
            
          </app-login-form>
        </div>

        <div class="reg-register-form__actions-block" *ngSwitchCase="actions.register">
          <app-otp-form class="form-holder" *ngIf="!otpVerified && !(extraOptions?.otpVerified)"
                        (otpVerified)="otpVerification($event)"
                        (forgotPassEvent)="forgotPassWord($event)"
                        [verification_guid]="verificationGuid">
          </app-otp-form>
          <div class="reg-actions-block__new-password" *ngIf="otpVerified || extraOptions?.otpVerified">
            <app-password-form
              [msisdn]="msisdn" [verification_guid]="verificationGuid"
              (registrationSuccess)="checkRegistrationSuccess($event)" (forgotPassEvent)="forgotPassWord($event)" class="form-holder"></app-password-form>
          </div>
        </div>
        <button class="cancel" (click)="closeClicked()">{{'cancel' | translate}}</button>

      </div>
    </div>
  `
})
export class RegistrationComponent implements OnInit, OnChanges {

  @Output()
  user: EventEmitter<IUser> = new EventEmitter<IUser>()

  @Output()
  registrationDone: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input()
  action: string;

  @Input()
  lang: string;

  @Input()
  user_guie: string;

  @Input()
  systemName: string;

  getActionTitle() {
    if (!this.userExists) {
      return 'registration_in';
    } else {
      return 'enter_to';
    }
  }

  @Output()
  onClose: EventEmitter<void> = new EventEmitter<void>();

  @Input()
  extraOptions: IExtraOptions = {};

  @Input()
  locales: ILocales = {};

  @Input()
  config: IServiceConfig;

  msisdn: string;
  password: string;

  registrationSuccess: boolean;
  userCredentials: IUserCheckResult;

  otpVerified: boolean;

  actions = {
    login: 'login',
    forgot: 'forgot',
    register: 'register',
    none: 'none'
  };

  userExists: boolean = false;
  userCheckDone: boolean = false;
  forgotPassword: boolean = false;

  constructor(private service: RegistrationService,
              private localeService: LocaleService) {
  }

  forgotPassWord(e: boolean){
    const userCheckResults$: Observable<IUserCheckResult> = this.service.forgotPassGetUser(this.msisdn)
    userCheckResults$.subscribe((results: IUserCheckResult) => {
      this.userCheckDone = true;
      if (!results) {
        this.userExists = false;
        return;
      }
      this.forgotPassword = e;
      this.userCredentials = results
      this.user_guie = results.verification_guid
      this.userExists = results.userExists;
    });
  }

  ngOnInit() {
    this.service.config = this.config;
    this.localeService.setLocale(this.lang);
    this.localeService.updateLocales(this.locales)
  }

  ngOnChanges() {
    this.service.config = this.config;
    this.localeService.setLocale(this.lang);
  }

  public setUserAction(userExists: boolean) {
    this.userExists = userExists;
  }

  getAction() {
    if (this.userCheckDone || this.extraOptions.userCheckDone) {
      if (this.registrationSuccess || this.extraOptions.registrationSuccess) {
      }
      if (this.forgotPassword) {
        return this.actions.register
      }
      return (this.userExists || this.extraOptions.userExists) ? this.actions.login : this.actions.register
    } else {
      return this.actions.none;
    }
  }

  get verificationGuid() {
    return this.userCredentials && this.userCredentials.verification_guid;
  }

  otpVerification(verified: boolean) {
    this.otpVerified = verified;
  }

  checkRegistrationSuccess(password: string) {
    this.registrationSuccess = true;

    const user$: Observable<any> = this.service.login(password, this.msisdn);
    user$.first().subscribe(data => {
      if (!data.wrongPassword) {
        setTimeout(() => this.user.emit(data), 2000);
      }
    });

    if (this.registrationSuccess) {
      setTimeout(() => this.registrationDone.emit(true), 2000);
    }
  }

  getUserCredentials(userData: IUserCheckResult) {
    this.userCheckDone = true;
    this.userCredentials = userData;
  }

  getMsisdn(msisdn: string) {
    this.msisdn = msisdn;
  }

  getUser(user: IUser) {
    if (user) {
      this.user.emit({...user, msisdn: this.msisdn});
    }
  }

  closeClicked() {
    this.onClose.emit();
  }
}
