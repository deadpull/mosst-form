interface ILoggerOptions {
  log: boolean;
  debug: boolean;
  warning: boolean;
  error: boolean;
  history: boolean;
}

class LoggerClass {
  public options: ILoggerOptions;

  private history: {params: {value: string, json: boolean}[], method: string}[];

  constructor() {
    this.options = {
      log: true,
      debug: false,
      warning: true,
      error: true,
      history: false
    };

    this.history = [];
  }

  public log(...params: any[]): void {
    this.write(params, 'log');
  }

  public debug(...params: any[]): void {
    if (this.options.debug) {
      this.write(params, 'debug');
    }
  }

  public warning(...params: any[]): void {
    this.write(params, 'warn');
  }

  public error(...params: any[]): void {
    this.write(params, 'error');
  }

  public forEach(callbackfn: (value: {params: any[], method: string}, index: number) => void): void {
    for (let i = 0; i < this.length(); ++i) {
      const item = this.read(i);
      callbackfn(item, i);
    }
  }

  public read(index: number): {method: string, params: any[]} {
    if (index >= this.history.length) {
      return;
    }

    const item = this.history[index];

    const {method} = item;

    const params = item.params.map(param =>
      param.json ? JSON.parse(param.value) : param.value
    );

    return {method, params};
  }

  public length(): number {
    return this.history.length;
  }

  public clear(): void {
    this.history = [];
  }

  private write(params: any[], method): void {
    params.unshift(`[${new Date().toString()}]`);
    console[method].apply(console, params);

    if (this.options.history) {
      this.saveToHistory(params, method);
    }
  }

  private saveToHistory(params, method): void {
    const _params = params.map(val => {
      const json = typeof(val) === 'object';

      return {
        value: json ? JSON.stringify(val) : val,
        json: json
      };
    });

    this.history.push({method, params: _params});
  }
}

export const Logger = new LoggerClass();
