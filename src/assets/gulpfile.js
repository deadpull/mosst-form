var gulp = require('gulp'),
	browserSync = require('browser-sync').create(),
	logger = require('gulp-logger'),
	flatten = require('gulp-flatten'),
	sourcemaps = require('gulp-sourcemaps'),
	autoprefixer = require('gulp-autoprefixer'),
	sass = require('gulp-sass');

gulp.task('serve', ['sass'], function () {

	browserSync.init({
		server: { // Определяем параметры сервера
//			proxy: ""
		}
	});

	gulp.watch("./scss/**/*.scss", ['sass']);

});

gulp.task('sass', function () {
	return gulp.src("./scss/style.scss")
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: [
				"last 2 version",
				"ie 10",
				"ios 6",
				"android 4"
			]
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest("../styles.css"))
		.pipe(browserSync.stream());
});

gulp.task('default', ['serve']);
