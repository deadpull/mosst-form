const mainRequest = require('request');
const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const crypto = require('crypto');
const path = require('path');
const _ = require('lodash');

const app = express();
const memberPointId = 9001;
const signKey = 'zmFgCY9QkNcJtCRdfXTDTksLLGq3GqHqne8FOmK1';

app.disable('x-powered-by');

app.use(bodyParser.json());
app.use(compression());
app.use(bodyParser.urlencoded({extended: true}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-  With, Content-Type, Accept");
  next();
});

app.options('*', (req, res) => {
  res.send();
});

const getAuthDate = () => {
  const date = new Date();
  date.setSeconds(date.getSeconds());
  const dateISO = date.toISOString();
  const sign = crypto.createHash('md5').update(memberPointId + dateISO + signKey).digest("hex");

  return {
    memberPointId: memberPointId,
    time: dateISO,
    sign: sign
  }
};

const reverseProxy = (BASE_URL, request, response) => {
  let data = Object.assign({}, {
    auth: getAuthDate()
  }, request.body);
  console.log('!!!!!!!!!');
  console.log(`${BASE_URL}${request.originalUrl}`);
  console.log(data);
  console.log('--------');
  makeRequest(response, {
    url: `${BASE_URL}${request.originalUrl}`,
    body: data
  });
};

app.post('/api/user/:path', (request, response) => {
  const {memberPointId, time, sign} = getAuthDate();
  const registerData = _.extend({}, {
      auth: {
        memberPointId: memberPointId,
        time,
        sign
      }
    },
    request.body);

  console.log('PATH: /api/user/', request.params.path);
  console.log(registerData);

  makeRequest(response, {
    url: `https://a-apiusers.mosst.com/${request.params.path}`,
    body: registerData
  });
});

app.post('/api/login', (request, response) => {
  const loginData = {
    auth: getAuthDate(),
    msisdn: '+380669336432',
    pin: '1984'
  };

  makeRequest(response, {
    url: 'http://a-users-api.azurewebsites.net/login',
    body: loginData
  });
});


app.post('/tmpl/*', reverseProxy.bind(null, 'http://a-users-api.azurewebsites.net'));
app.post('/api/*', reverseProxy.bind(null, 'https://a-apicash.mosst.com'));

if (app.get('env') === 'production') {

  // in production mode run application from dist folder
  app.use(express.static(path.join(__dirname, '/../client')));
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  let err = new Error('Not Found');
  next(err);
});

// production error handler
// no stacktrace leaked to user
app.use(function (err, req, res, next) {
  console.log(err);
  res.status(err.status || 500);
  res.json({
    error: {},
    message: err.message
  });
});

function makeRequest(response, {url, body, json = true}) {
  mainRequest.post(
    {url, body, json},
    (err, res, body) => {
      if (res.statusCode !== 200) {
        response.json({error: {name: 'API is not available', code: -1}});
      }
      if (!body) {
        return response.send();
      }
      console.log('!!!!');
      console.log(url);
      console.log(body.response || body);
      console.log('_________');

      try {
        response.json(body.response || body);
      } catch (err) {
        response.end(err);
      }
    }
  );
}

module.exports = app;
